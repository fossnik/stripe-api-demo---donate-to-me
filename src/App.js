import React from 'react';
import logo from './logo.svg';
import './App.css';
import StripeCheckout from 'react-stripe-checkout';

function App() {
  const [product] = React.useState({
    name: 'Donation',
    price: 5.00,
  });

  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo'/>
        <h1 className='App-title'>Welcome to React</h1>
      </header>
      <p className='App-intro'>
        To give me a donation, pay here:
        <StripeCheckout
          stripeKey={}
        />
      </p>
    </div>
  );
}

export default App;
